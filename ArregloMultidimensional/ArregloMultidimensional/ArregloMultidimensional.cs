﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloMultidimensional
{
    class ArregloMultidimensional
    {
        static void Main(string[] args)
        {
            //Declaramos el arreglo multidimensional de 3 filas por 5 columnas, esto hace un total de 15 indices
            int[,] numeros = new int[3, 5];

            //Solicitamos un número al usuario para cada uno de los 15 índices
            for (int i = 0; i < 3; i++)//Itera sobre las 3 filas del arreglo multidimensional
            {
                for (int j = 0; j < 5; j++)//Itera sobre las 5 columnas de cada fila
                {
                    Console.Write($"Ingrese un número en la posició [{i},{j}]: ");
                    numeros[i, j] = int.Parse(Console.ReadLine());
                }
            }

            Console.WriteLine("\n\n");

            //El proceso para mostrar la información almacenada en cada índice es el mismo que para almacenarlos
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.WriteLine($"El valor en la posición [{i},{j}] = {numeros[i, j]}");
                }
            }

            Console.WriteLine("\n\n<<<<<ARREGLO MULTIDIMENSIONAL DE 3X5>>>>>");

            //Dibujamos el arreglo multidimensional de 3x5
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write($"{numeros[i, j]}");

                    if (j != 4)
                    {
                        Console.Write("|");
                    }
                }

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}